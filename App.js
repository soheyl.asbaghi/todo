import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Alert,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import uuid from 'react-native-uuid';

import Header from './components/Header';
import TodoItem from './components/TodoItem';
import AddTodo from './components/AddTodo';

const App = () => {
  const [todos, setTodos] = useState([
    {text: 'Go to Gym', id: uuid.v4()},
    {text: 'Wash the dishes', id: uuid.v4()},
    {text: 'Go to shopping', id: uuid.v4()},
    {text: 'Create an app', id: uuid.v4()},
  ]);

  const deleteItemHandler = id => {
    setTodos(prevItem => {
      return prevItem.filter(todo => todo.id !== id);
    });
  };

  const addItemHndler = text => {
    if (!text) {
      Alert.alert('OOPS!', 'Please enter an todo', [{text: 'OK'}]);
    } else {
      setTodos(prevItem => {
        return [...prevItem, {id: uuid.v4(), text: text}];
      });
    }
  };

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}>
      <View style={styles.container}>
        <Header />
        <View style={styles.content}>
          <AddTodo addItemHndler={addItemHndler} />
          <View style={styles.list}>
            <FlatList
              keyExtractor={item => item.id}
              data={todos}
              renderItem={({item}) => (
                <TodoItem item={item} deleteItemHandler={deleteItemHandler} />
              )}
            />
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eee',
  },
  content: {
    padding: 40,
  },
  list: {
    marginTop: 12,
  },
});

export default App;
