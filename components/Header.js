import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Header = () => {
  return (
    <View style={styles.header}>
      <Text style={styles.title}>Todo list</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    height: 60,
    padding: 16,
    backgroundColor: 'coral',
  },
  title: {
    fontSize: 23,
    color: '#fff',
    textAlign: 'center',
    fontWeight: 'bold',
  },
});

export default Header;
