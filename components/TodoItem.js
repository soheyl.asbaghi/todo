import React from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

const TodoItem = ({item, deleteItemHandler}) => {
  return (
    <TouchableOpacity style={styles.item}>
      <Text style={styles.title}>{item.text}</Text>
      <Icon
        name="close"
        size={18}
        color="#fff"
        onPress={() => deleteItemHandler(item.id)}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  item: {
    padding: 8,
    marginTop: 15,
    backgroundColor: 'coral',
    borderRadius: 8,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    color: '#fff',
    fontSize: 16,
  },
});

export default TodoItem;
