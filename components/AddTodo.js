import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Text,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

const AddTodo = ({addItemHndler}) => {
  const [text, setText] = useState('');

  const changeHandler = value => {
    setText(value);
  };
  return (
    <View>
      <TextInput
        style={styles.input}
        placeholder="New Todo..."
        onChangeText={changeHandler}
      />
      <TouchableOpacity style={styles.btn} onPress={() => addItemHndler(text)}>
        <Text style={styles.btnText}>
          <Icon name="plus" size={16} color="#fff" />
          Add Todo
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    height: 60,
    padding: 8,
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#aaa',
  },
  btn: {
    backgroundColor: 'coral',
    padding: 9,
    margin: 5,
  },
  btnText: {color: '#fff', fontSize: 16, textAlign: 'center'},
});

export default AddTodo;
